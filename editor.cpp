
#include <GL/gl3w.h>

#include <SDL.h>
#include <imgui.h>
#include <sstream>
#include <stb_rect_pack.h>
#include <stdint.h>
#include <string.h>
#include <string>
#include <tinyfd/tinyfiledialogs.h>
#include <vector>

// strtkok_r
#include <string.h>

// dlopen, ...
#include <dlfcn.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define STB_RECT_PACK_IMPLEMENTATION
#define STBRP_STATIC
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_rect_pack.h"
#include "stb_truetype.h"

#define SOLEIL__FOXUI_IMPLEMENTATION 1
#define FOXUI_STB_ALREADY_INCLUDED 1
#include <foxui/foxui.h>

#include <editor.h>

// TODO: Replace printf with an error logger
// TODO: Add format ("%s", ...) to SetErrorMessage and add errno message.
// TODO: Replace SetErrorMessage in File_* and Image_* lib with an error accesss
// function (e.g. Image_GetError())

/** Same as strncpy except that the last byte is always a null-char. Returns a
 * positive number if all bytes where copied. Negative if not all where copied
 * and 0 if `n` == 0.*/
int String_NCopy(char *__restrict dest, const char *__restrict src, size_t n);

int String_NCopy(char *dest, const char *src, size_t n) {
  if (n == 0) {
    return 0;
  }

  dest[n - 1] = '\0';
  strncpy(dest, src, n);
  if (dest[n - 1] != '\0') {
    dest[n - 1] = '\0';
    return -1;
  }
  return 1;
}

/** Opaque type to handle dynamic libraries. */
typedef struct {
  void *handle;
} Module;

/** Open a Shared Library (or dynamically linked). */
int Module_Load(const char *path, Module *module);

/** Retrieve the function of the shared library. In case of error return NULL.
 */
void *Module_GetSymbol(Module *module, const char *symbol);

/** Close and release the shared module. */
int Module_Release(Module *module);

int Module_Load(const char *path, Module *module) {
  module->handle = dlopen(path, RTLD_NOW);
  if (module->handle == NULL) {
    // FoxuiEditor::SetErrorMessage("Cannot open Module");
    FoxuiEditor::SetErrorMessage(dlerror());
    return -1;
  }
  return 0;
}

void *Module_GetSymbol(Module *module, const char *symbol) {
  void *ret;
  const char *error;

  ret = dlsym(module->handle, symbol);
  error = dlerror();
  if (error != NULL) {
    FoxuiEditor::SetErrorMessage(error);
    return NULL;
  }
  return ret;
}

int Module_Release(Module *module) {
  int ret;

  if (module->handle == NULL) {
    return 0;
  }
  ret = dlclose(module->handle);
  module->handle = NULL;
  return ret;
}

/** Output the text in content in a file. Overwriting it if one already exists.
 * Return 0 upon success or something < 0 in case of error.*/
int File_Write(const char *path, const char *content);

/** Fully read the file and return it as a new allocated pointer. Return NULL in
 * case of failure. This function is not for opening big files. Those bigger
 * than 4 MB will not be read. */
char *File_ReadFull(const char *path);

int File_Write(const char *path, const char *content) {
  FILE *file;
  size_t ret;
  const size_t content_length = strlen(content);

  file = fopen(path, "w");
  if (file == NULL) {
    FoxuiEditor::SetErrorMessage("Failed to open file for write.");
    return -1;
  }
  ret = fwrite(content, sizeof(*content), content_length, file);
  if (ret < (sizeof(*content) * content_length)) {
    FoxuiEditor::SetErrorMessage("Failed to write file.");
    fclose(file);
    return -1;
  }
  ret = fclose(file);
  if (ret == 0) {
    return 0;
  } else {
    FoxuiEditor::SetErrorMessage("Failed to flush file.");
    return -1;
  }
}

// TODO: Not the optimal way to read a text file.
char *File_ReadFull(const char *path) {
  FILE *file = NULL;
  const size_t file_length_max = 4096u * 1000u * 1000u; /* 4MB */
  size_t file_length = 0;
  size_t length_read;
  char *content = NULL;
  int ret;

  file = fopen(path, "rb");
  if (file == NULL) {
    FoxuiEditor::SetErrorMessage("Cannot open file for read.");
    goto ReleaseAndExit;
  }
  ret = fseek(file, 0, SEEK_END);
  if (ret != 0) {
    FoxuiEditor::SetErrorMessage("Unable to get the file size (the file might "
                                 "be too large or not be a standard one).");
    goto ReleaseAndExit;
  }
  file_length = ftell(file);
  rewind(file);
  if (file_length > file_length_max) {
    FoxuiEditor::SetErrorMessage("File is too big to be opened.");
    goto ReleaseAndExit;
  }
  content = (char *)malloc(file_length + 1);
  if (content == NULL) {
    FoxuiEditor::SetErrorMessage("Not enough memory.");
    goto ReleaseAndExit;
  }
  length_read = fread(content, 1, file_length, file);
  if (length_read != file_length) {
    free(content);
    FoxuiEditor::SetErrorMessage("File not fully read.");
    goto ReleaseAndExit;
  }
  content[file_length] = '\0';

ReleaseAndExit:
  if (file != NULL) {
    fclose(file);
  }
  return content;
}

struct Image {
  int width;
  int height;
  /** Number of 8-bit component per pixel (e.g. 4 == RBA) */
  int channels;
  stbi_uc *data;
};

/** Load an image, return something < 0 in case of error. */
int Image_Load(Image *image, const char *path);
/** Create (allocate) a RGBA Image */
int Image_CreateRGBA(Image *image, const int width, const int height);
/** Import an image to OpenGl */
GLuint Image_ToGLTexture(Image *image);
/** Copy part of a source image to the dest one. Images cannot overlap. If
 * `source_rec` is NULL, the whole image is copied. if `dest_rec` is smaller
 * than `source_rec` only this part of the image is copied. */
int Image_Copy(Image *source, foxui_rectangle *source_rec, Image *dest,
               foxui_rectangle *dest_rec);

int Image_Load(Image *image, const char *path) {
  if (image == NULL) {
    printf("Cannot pass NULL as image parameter");
    return -1;
  }

  // TODO: Image_Copy seems not to work with different channels
  const int number_channels = 0;
  if (number_channels > 0) {
    image->data = stbi_load(path, &(image->width), &(image->height),
                            &(image->channels), 4);
    image->channels = 4;
  } else {
    image->data = stbi_load(path, &(image->width), &(image->height),
                            &(image->channels), 0);
  }
  if (image->data == NULL) {
    // TODO: error management from STB
    printf("Failed to open image: '%s'.\n", path);
  }
  return 0;
}

GLuint Image_ToGLTexture(Image *image) {
  GLenum format;
  GLuint tex = 0;

  if (image == NULL) {
    printf("Cannot pass NULL as image parameter");
    return 0;
  }
  switch (image->channels) {
  case 1:
    format = GL_RED;
    break;
  case 2:
    format = GL_RG;
    break;
  case 3:
    format = GL_RGB;
    break;
  case 4:
    format = GL_RGBA;
    break;
  }

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
  glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
  glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

  glGenTextures(1, &tex);
  if (tex == 0) {
    // TODO: Error management from OpenGL
    return 0;
  }
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, format, image->width, image->height, 0, format,
               GL_UNSIGNED_BYTE, image->data);

  // TODO: Here check for opengl log.

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

  return tex;
}

int Image_CreateRGBA(Image *image, const int width, const int height) {
  const int length = width * height * 4;
  const size_t size = sizeof(stbi_uc) * length;

  if (image == NULL) {
    printf("Image parameter cannot be NULL");
    return -1;
  }
  image->width = width;
  image->height = height;
  image->channels = 4;
  image->data = (stbi_uc *)malloc(size);
  if (image->data == NULL) {
    printf("Memory allocation failed");
    return -1;
  }
  memset(image->data, 0x00, length);
  return 0;
}

int Image_Copy(Image *source, foxui_rectangle *source_rec, Image *dest,
               foxui_rectangle *dest_rec) {
  foxui_rectangle source_rec_full;

  if (source == NULL || dest == NULL || dest_rec == NULL) {
    printf("Parameters images source and dest along with des rectangle cannot "
           "be null");
    return -1;
  }
  if (source_rec == NULL) {
    source_rec_full.x = 0;
    source_rec_full.y = 0;
    source_rec_full.width = source->width;
    source_rec_full.height = source->height;
    source_rec = &source_rec_full;
  }
  if (source_rec->x + source_rec->width > source->width ||
      source_rec->y + source_rec->height > source->height ||
      dest_rec->x + dest_rec->width > dest->width ||
      dest_rec->y + dest_rec->height > dest->height) {
    printf("Source or Destination image will be have copied part outside of "
           "their range.\n");
    return -1;
  }
  for (int y = 0; y < source_rec->height; ++y) {
    if (y >= dest_rec->height) {
      break;
    }
    for (int x = 0; x < source_rec->width; ++x) {
      const int src_x = source_rec->x + x;
      const int src_y = source_rec->y + y;
      const int dest_x = dest_rec->x + x;
      const int dest_y = dest_rec->y + y;
      const int dest_r =
          (dest_y * dest->channels * dest->width + dest_x * dest->channels);
      const int src_r =
          (src_y * source->channels * source->width + src_x * source->channels);

      if (x >= dest_rec->width) {
        continue;
      }
      for (int c = 0; c < dest->channels; ++c) {
        stbi_uc pixel = (c < source->channels) ? source->data[src_r + c] : 255;
        dest->data[dest_r + c] = pixel;
      }
    }
  }
  return 0;
}

static void HelpMarker(const char *desc) {
  ImGui::TextDisabled("(?)");
  if (ImGui::IsItemHovered()) {
    ImGui::BeginTooltip();
    ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
    ImGui::TextUnformatted(desc);
    ImGui::PopTextWrapPos();
    ImGui::EndTooltip();
  }
}

namespace {
static const char *s_WidgetList[] = {"foxui_widgetlist_textbox"};

static const char *s_PartsList[] = {
    "foxui_widgetpart_topleft",     "foxui_widgetpart_top",
    "foxui_widgetpart_topright",    "foxui_widgetpart_left",
    "foxui_widgetpart_content",     "foxui_widgetpart_right",
    "foxui_widgetpart_bottomleft",  "foxui_widgetpart_bottom",
    "foxui_widgetpart_bottomright",
};
} // namespace

static std::string BuildCConfiguration(foxui_param *param) {
  std::stringstream str;

  // TODO: FIXME: Just a temp hack to make the DLL work.
  str << "#include <stdlib.h>\n";
  str << "#include \"stb_rect_pack.h\"\n";
  str << "#include \"stb_truetype.h\"\n";
  str << "#define FOXUI_STB_ALREADY_INCLUDED 1\n";
  str << "#include \"../foxui/foxui.h\"\n";
  str << "\n\n";

  str << "void foxui_configure_lnf(foxui_param* param)\n"
      << "{\n";

  str << "  param->atlas_width = " << std::to_string(param->atlas_width)
      << ";\n";
  str << "  param->atlas_height = " << std::to_string(param->atlas_height)
      << ";\n";

  str << "  param->user_image_count = "
      << std::to_string(param->user_image_count) << ";\n";

  str << "param->user_images = (foxui_rectangle*)foxui_malloc("
         "sizeof(*param->user_images) * param->user_image_count);\n";
  for (size_t i = 0; i < param->user_image_count; ++i) {
    str << "param->user_images[" + std::to_string(i)
        << "].x = " << std::to_string(param->user_images[i].x) << ";\n";
    str << "param->user_images[" + std::to_string(i)
        << "].y = " << std::to_string(param->user_images[i].y) << ";\n";
    str << "param->user_images[" + std::to_string(i)
        << "].width = " << std::to_string(param->user_images[i].width) << ";\n";
    str << "param->user_images[" + std::to_string(i)
        << "].height = " << std::to_string(param->user_images[i].height)
        << ";\n";
  }

  str << "param->sdf_values[0] = " << std::to_string(param->sdf_values[0])
      << ";\n"
      << "param->sdf_values[1] = " << std::to_string(param->sdf_values[1])
      << ";\n"
      << "param->lnf_list_count = " << std::to_string(param->lnf_list_count)
      << ";\n"
      << "param->lnf_list      = "
         "(foxui_widget_lnf*)malloc(sizeof(*param->lnf_list) *"
      << " param->lnf_list_count);\n"
      << "\n";

  for (size_t w = 0; w < param->lnf_list_count; ++w) {
    foxui_widget_lnf *lnf = &(param->lnf_list[w]);

    const std::string widgetName =
        (w < foxui_widgetlist_count) ? s_WidgetList[w] : std::to_string(w);
    const std::string nbPartStr = std::to_string(lnf->nb_parts);

    str << "foxui_widget_lnf* tb = &(param->lnf_list[" << widgetName << "]);\n"
        << "tb->nb_parts         = " << nbPartStr << ";\n";

    str << "tb->parts_size =\n"
        << "  (foxui_rectangle*)malloc(sizeof(*tb->parts_size) * " << nbPartStr
        << ");\n"
        << "tb->parts_uv = (foxui_rectangle*)malloc(sizeof(*tb->parts_uv) * "
        << nbPartStr << ");\n"
        << "\n"
        << "bzero(tb->parts_size, sizeof(*tb->parts_size) * " << nbPartStr
        << ");\n"
        << "bzero(tb->parts_uv, sizeof(*tb->parts_uv) * " << nbPartStr
        << ");\n";

    for (size_t p = 0; p < lnf->nb_parts; ++p) {
      const std::string partName =
          (p < foxui_widgetpart_count) ? s_PartsList[p] : std::to_string(p);
      const foxui_rectangle *part = &(lnf->parts_size[p]);
      const foxui_rectangle *uv = &(lnf->parts_uv[p]);

      str << "tb->parts_size[" << partName
          << "].x      = " << std::to_string(part->x) << ";\n";
      str << "tb->parts_size[" << partName
          << "].y      = " << std::to_string(part->y) << ";\n";
      str << "tb->parts_size[" << partName
          << "].width  = " << std::to_string(part->width) << ";\n";
      str << "tb->parts_size[" << partName
          << "].height = " << std::to_string(part->height) << ";\n";

      str << "tb->parts_uv[" << partName
          << "].x      = " << std::to_string(uv->x) << ";\n";
      str << "tb->parts_uv[" << partName
          << "].y      = " << std::to_string(uv->y) << ";\n";
      str << "tb->parts_uv[" << partName
          << "].width  = " << std::to_string(uv->width) << ";\n";
      str << "tb->parts_uv[" << partName
          << "].height = " << std::to_string(uv->height) << ";\n";
    }
  }
  str << "}\n";
  return str.str();
}

struct ImFoxUIEditor_data {
  float zoom = 1.0f;
  /** Allow user to move the image vertically and horizontally */
  float offset[2] = {0.0f, 0.0f};
  int selectedWidget = 0;
  std::vector<char const *> widgetList = {"TextBox"};
  int widgetCount = foxui_widgetlist_count;

  /** Position pointer to the atlas image */
  ImVec2 atlas_pos;

  /** Size of the atlas image display. */
  ImVec2 atlas_size = ImVec2(512.0f, 512.0f);

  /** Drawlist pointing on the atlas */
  ImDrawList *draw_list = NULL;

  /** Image currently used as an atlas in OpengGL. */
  Image atlas;
};

/** Editor Global config */
static ImFoxUIEditor_data g_data;

void ImFoxUIEditor_PushPart(foxui_param *param,
                            ImFoxUIEditor_data const &data) {
  foxui_widget_lnf *lnf = &(param->lnf_list[data.selectedWidget]);

  lnf->nb_parts++;
  const size_t total = [param]() {
    size_t t = 0;
    for (size_t i = 0; i < param->lnf_list_count; ++i) {
      t += param->lnf_list[i].nb_parts;
    }
    return t;
  }();

  // TODO: Use total when part will have only one buffer
  lnf->parts_size = (foxui_rectangle *)realloc(
      lnf->parts_size, sizeof(*lnf->parts_size) * lnf->nb_parts);
  lnf->parts_uv = (foxui_rectangle *)realloc(
      lnf->parts_uv, sizeof(*lnf->parts_uv) * lnf->nb_parts);
  lnf->parts_size[lnf->nb_parts - 1] =
      foxui_sub_construct_rectangle(0, 0, 0, 0);
  lnf->parts_uv[lnf->nb_parts - 1] = foxui_sub_construct_rectangle(0, 0, 0, 0);
}

void ImFoxUIEditor_WidgetConf(foxui_param *param,
                              ImFoxUIEditor_data const &data) {

  if (data.selectedWidget >= 0 && data.selectedWidget < param->lnf_list_count) {

    static int selectedParts = 0;
    const ImU32 outlineColor = ImColor(1.0f, 1.0f, 0.4f, 1.0f);
    const ImU32 selectedOutlineColor = ImColor(1.0f, 1.0f, 0.4f, 0.3f);
    ImDrawCornerFlags corners_none = 0;
    float th = 1.0f;
    const float scale = data.atlas_size.x * data.zoom;

    // TODO: Now scale must include a scaleX and a scaleY.
    foxui_widget_lnf *lnf = &(param->lnf_list[data.selectedWidget]);

    ImGui::ListBox("Part", &selectedParts, s_PartsList, lnf->nb_parts);

    printf("%i > %i\n", data.selectedWidget, foxui_widgetlist_count);
    if (data.selectedWidget >= foxui_widgetlist_count) {
      // User defined widgets
      // ImGui::SameLine();
      if (ImGui::Button("Add part")) {
        ImFoxUIEditor_PushPart(param, data);
      }

      ImGui::Columns(2, nullptr, true);
      for (size_t i = 0; i < lnf->nb_parts; ++i) {
        foxui_rectangle *part = &(lnf->parts_size[i]);
        foxui_rectangle *uv = &(lnf->parts_uv[i]);

        ImGui::PushID(i);
        ImVec2 origin(data.atlas_pos.x + (uv->x - data.offset[0]) * scale,
                      data.atlas_pos.y + (uv->y - data.offset[1]) * scale);
        ImVec2 extent(origin.x + (uv->width * scale),
                      origin.y + (uv->height * scale));

        data.draw_list->PushClipRect(
            data.atlas_pos,
            ImVec2(data.atlas_pos.x + data.atlas_size.x,
                   data.atlas_pos.y + data.atlas_size.y),
            true);
        if ((size_t)selectedParts == i) {
          ImGui::PushID("UV_parts");
          {
            ImGui::Text("UV");
            ImGui::SameLine();
            HelpMarker("Customize the UV setting for this widget part");
            ImGui::SliderFloat("x", &uv->x, 0.0f, 1.0f);
            ImGui::SliderFloat("y", &uv->y, 0.0f, 1.0f);
            ImGui::SliderFloat("width", &uv->width, 0.0f, 1.0f);
            ImGui::SliderFloat("height", &uv->height, 0.0f, 1.0f);
          }
          ImGui::PopID();

          // Second column
          ImGui::NextColumn();

          ImGui::PushID("Sizes");
          {
            ImGui::Text("Sizes");
            ImGui::SameLine();
            HelpMarker("Customize the size setting for this widget part");
            ImGui::SliderFloat("x", &part->x, 0.0f, 1.0f);
            ImGui::SliderFloat("y", &part->y, 0.0f, 1.0f);
            ImGui::SliderFloat("width", &part->width, 0.0f, 1.0f);
            ImGui::SliderFloat("height", &part->height, 0.0f, 1.0f);
          }
          ImGui::PopID();

          // First column
          ImGui::NextColumn();

          data.draw_list->AddRectFilled(origin, extent, selectedOutlineColor);
        } else {
          data.draw_list->AddRect(origin, extent, outlineColor, 0.0f,
                                  corners_none, th);
        }

        ImGui::PopID(/* i */);
      }
      ImGui::Columns(1);
      // draw_list->AddRectFilled(ImVec2(x, y), ImVec2(x + sz, y + sz), col);

      ImGui::BeginChild("Auto-configure");
      {
        ImGui::Text("Auto-configure Area");
        ImGui::Text("If the widget is composed of one block, multiples part "
                    "that stick together as in the usual window ui, the "
                    "settings can be automatically computed from the size of "
                    "the widget and its margins.");

        // Settings for auto-configuring the size.
        static float verticalMargins = 0.1f;
        static float horizontalMargins = 0.1f;
        static float ratio = 0.6f;

        // Settings for auto-customizing the UV.
        const ImU32 uvOutlineColor = ImColor(224, 0, 205, 255);
        static bool includeUV = false;
        static float uvVerticalMargins = 0.1f;
        static float uvHorizontalMargins = 0.1f;
        static float uvSize[2] = {0.6f, 0.6f};
        static float uvOffset[2] = {0.0f, 0.0f};

        ImGui::Columns(2, nullptr, true);

        ImGui::PushItemWidth(80);
        {
          ImGui::InputFloat("Vertical Margins", &verticalMargins);
          ImGui::SameLine();
          HelpMarker("Size (relative to ratio) of the left and right panels.");

          ImGui::InputFloat("Horizontal Margins", &horizontalMargins);
          ImGui::SameLine();
          HelpMarker("Size (relative to ratio) of the top and bottom panels.");

          ImGui::InputFloat("Ratio", &ratio);
          ImGui::SameLine();
          HelpMarker("Size of the widget.");
        }
        ImGui::PopItemWidth();
        ImGui::NextColumn();

        ImGui::Checkbox("Include UV", &includeUV);
        ImGui::SameLine();
        HelpMarker("If the UV in the atlas texture is also contiguous, it "
                   "can also be configured automatically.");

        if (includeUV) {
          ImGui::PushItemWidth(80);
          {

            ImGui::InputFloat("UV Vertical Margins", &uvVerticalMargins);
            ImGui::InputFloat("UV Horizontal Margins", &uvHorizontalMargins);
          }
          ImGui::PopItemWidth();

          ImGui::SliderFloat2("UV Size", uvSize, 0.0f, 1.0f);
          ImGui::SliderFloat2("UV offset", uvOffset, 0.0f, 1.0f);

          ImVec2 origin(
              data.atlas_pos.x + ((uvOffset[0] - data.offset[0]) * scale),
              data.atlas_pos.y + ((uvOffset[1] - data.offset[1]) * scale));
          ImVec2 extent(origin.x + (uvSize[0] * scale),
                        origin.y + (uvSize[1] * scale));
          data.draw_list->PushClipRect(
              data.atlas_pos,
              ImVec2(data.atlas_pos.x + data.atlas_size.x,
                     data.atlas_pos.y + data.atlas_size.y),
              true);
          data.draw_list->AddRect(origin, extent, uvOutlineColor, 0.0f,
                                  corners_none, th);
        }
        ImGui::Columns(1);

        if (lnf->nb_parts < foxui_widgetpart_count) {
          ImGui::Text("Widget must have, at least %i parts to represent a "
                      "nominal widget.",
                      foxui_widgetpart_count);
        } else {
          if (ImGui::Button("Compute size")) {
            const float verticalSize = ratio - (ratio * verticalMargins * 2.0f);
            const float horizontalSize =
                ratio - (ratio * horizontalMargins * 2.0f);

            const float uvVerticalSize = uvSize[0] - (uvVerticalMargins * 2.0f);
            const float uvHorizontalSize =
                uvSize[1] - (uvHorizontalMargins * 2.0f);

            float y = 0.0f;
            float uvY = 0.0f;
            for (size_t i = 0; i < lnf->nb_parts; i += 3) {
              float x = 0;
              float uvX = 0;
              for (size_t j = 0; j < 3; ++j) {
                foxui_rectangle *part = &(lnf->parts_size[i + j]);

                part->x = x;
                part->y = y;
                part->width = (j == 1) ? verticalSize : verticalMargins;
                part->height = (i == 3) ? horizontalSize : horizontalMargins;

                if (includeUV) {
                  foxui_rectangle *uv = &(lnf->parts_uv[i + j]);

                  uv->x = uvOffset[0] + uvX;
                  uv->y = uvOffset[1] + uvY;
                  uv->width = (j == 1) ? uvVerticalSize : uvVerticalMargins;
                  uv->height =
                      (i == 3) ? uvHorizontalSize : uvHorizontalMargins;

                  uvX += uv->width;
                }

                x += part->width;
              }

              y += (i == 0) ? horizontalMargins : horizontalSize;
              uvY += (i == 0) ? uvHorizontalMargins : uvHorizontalSize;
            }
          }
        }
      }
      ImGui::EndChild(); // Auto-configure
    }
  }
}

#if 0
  // TODO: When I've chosen to keep or not lnf
void
ImFoxUIEditor_ImportImage(foxui_param* param, ImFoxUIEditor_data const& data)
{
  // Block the main thread?
  std::string imagePath = ImFoxUIEditor_OpenDialog();
  if (imagePath.empty()) { return; }

  
}
#endif

void ImFoxUIEditor_UserImage(foxui_param *param,
                             ImFoxUIEditor_data const &data) {
  static int currentItem = 0;
  static std::vector<std::string> items = {};

  items.clear();
  for (size_t i = 0; i < param->user_image_count; ++i) {
    items.push_back(std::string("Image N. ") + std::to_string(i));
  }
  auto GetItem = [](void *data, int idx, const char **out_text) {
    (void)data;
    *out_text = items[idx].c_str();
    return *out_text != NULL;
  };
  ImGui::ListBox("User Images", &currentItem, GetItem, nullptr, items.size());
  ImGui::SameLine();
  if (ImGui::Button("New Image")) {
    FoxuiEditor::IncrementUserImages(param);
  }
  ImGui::SameLine();
  if (ImGui::Button("Import")) {
    // TODO: Allow multiple select.
    const char *file =
        tinyfd_openFileDialog("Open Image", NULL, 0, NULL, "Image files", 1);
    if (file) {
      const size_t pathLengthMax = 4096;
      char path[pathLengthMax];
      char* t = &path[0];
      char *ptr;
      char *str;
      int ret;

      // TODO: Calling multiple times ImportSingleImage is less efficient that writing a ImportMultiples function. But not enough time to write it and it's not in a critical path of the program.
      // TODO: Review this code
      ret = String_NCopy(path, file, pathLengthMax);
      if (ret < 0) {
        FoxuiEditor::SetErrorMessage("Path too long.");
      } else {
        // TODO: Might not handle multi-bytes.
        while ((str = strtok_r(t, "|", &ptr)) != NULL) {
          FoxuiEditor::ImportSingleImage(param, str);
	  t = NULL;
        }
      }
    }
  }

#if 0
  // TODO: When I've chosen to keep or not lnf
  if (ImGui::Button("Import Image")) { ImFoxUIEditor_ImportImage(); }
#endif

  if (items.size() > 0) {
    const float scaleX = data.atlas_size.x * data.zoom;
    const float scaleY = data.atlas_size.y * data.zoom;
    ImDrawCornerFlags corners_none = 0;
    float th = 1.0f;
    const ImU32 uvOutlineColor = ImColor(224, 0, 205, 255);
    foxui_rectangle &img = param->user_images[currentItem];

    {
      ImGui::SliderFloat2("UV offset", &(img.x), 0.0f, 1.0f);
      ImGui::SliderFloat2("UV Size", &(img.width), 0.0f, 1.0f);
    }

    {
      // TODO: There must be now a scaleX amd scaleY
      data.draw_list->PushClipRect(data.atlas_pos,
                                   ImVec2(data.atlas_pos.x + data.atlas_size.x,
                                          data.atlas_pos.y + data.atlas_size.y),
                                   true);

      ImVec2 origin(data.atlas_pos.x + ((img.x - data.offset[0]) * scaleX),
                    data.atlas_pos.y + ((img.y - data.offset[1]) * scaleY));
      ImVec2 extent(origin.x + (img.width * scaleX),
                    origin.y + (img.height * scaleY));

      data.draw_list->AddRect(origin, extent, uvOutlineColor, 0.0f,
                              corners_none, th);
    }
  }
}

void ImFoxUIChooseEasing(foxui_param *param) {
  const std::vector<const char *> items = {
      "No Easing",         "Elastic ease in", "Elastic ease out",
      "Quintic ease out",  "Bounce ease out", "Bounce ease in",
      "Bounce ease in out"};
  ImGui::Combo("Easing method", &(param->sdfeasing), items.data(),
               items.size());
}

/// An editor for the foxui lib. currently made with ImGUI, I hope it to
/// remake it with foxui when it will be achieved.
void ImFoxUIEditor(foxui_param *param, foxui_renderdata *render) {
  ImGui::Begin("FoxUI");
  {
    ImFoxUIChooseEasing(param);
    ImGui::SliderFloat2("SDF Values", param->sdf_values, 0.01, 0.8);
    ImGui::ListBox("Widget", &g_data.selectedWidget,
                   [](void *ptr, int idx, const char **out_text) {
                     if (idx < foxui_widgetlist_count) {
                       *out_text = g_data.widgetList[idx];
                     } else {
                       *out_text = "User defined";
                     };
                     return *out_text != NULL;
                   },
                   g_data.widgetList.data(), param->lnf_list_count);
    ImGui::SameLine();
    if (ImGui::Button("Add widget")) {
      param->lnf_list_count++;
      // TODO: foxui_realloc()
      param->lnf_list = (foxui_widget_lnf *)realloc(
          param->lnf_list, sizeof(*param->lnf_list) * param->lnf_list_count);
      param->lnf_list[param->lnf_list_count - 1].nb_parts = 0;
      param->lnf_list[param->lnf_list_count - 1].parts_size = nullptr;
      param->lnf_list[param->lnf_list_count - 1].parts_uv = nullptr;
    }
    ImGui::SliderFloat("Zoom", &g_data.zoom, 1.0f, 10.0f);
    ImGui::SliderFloat2("Offset", g_data.offset, 0.0f, 1.0f);
    ImGui::SameLine();
    HelpMarker("Use the Zoom to enlarge the image, then the offset to move the "
               "focused zone.");
    {
      static float atlasSize[] = {param->atlas_width, param->atlas_height};
      if (ImGui::InputFloat2("Atlas size", atlasSize)) {
        // param->atlas_width  = atlasSize[0];
        // param->atlas_height = atlasSize[1];
      }
      if (ImGui::Button("Recompute UV")) {
        const float ratioX = param->atlas_width / atlasSize[0];
        const float ratioY = param->atlas_height / atlasSize[1];

        for (size_t i = 0; i < param->user_image_count; i++) {
          foxui_rectangle *userImg = &(param->user_images[i]);

          userImg->x *= ratioX;
          userImg->y *= ratioY;
          userImg->width *= ratioX;
          userImg->height *= ratioY;
        }

        for (size_t i = 0; i < param->lnf_list_count; i++) {
          foxui_widget_lnf *lnf = &(param->lnf_list[i]);

          for (size_t j = 0; j < lnf->nb_parts; j++) {
            foxui_rectangle *part = &(lnf->parts_uv[j]);

            part->x *= ratioX;
            part->y *= ratioY;
            part->width *= ratioX;
            part->height *= ratioY;
          }
        }

        param->atlas_width = atlasSize[0];
        param->atlas_height = atlasSize[1];
      }
    }
    if (ImGui::Button("Print C configuration on TTY")) {
      const std::string v = BuildCConfiguration(param);
      printf("%s", v.c_str());
      FoxuiEditor::SaveConfiguration(param, "foxui_configuration.c",
                                     "foxui_atlas.png");
    }

    // const ImVec2 p = ImGui::GetCursorScreenPos();

    // Save the current position where we draw the image and the current
    // draw_list.
    g_data.draw_list = ImGui::GetWindowDrawList();
    g_data.atlas_pos = ImGui::GetCursorScreenPos();
    g_data.atlas_size.y =
        param->atlas_height * (g_data.atlas_size.x / param->atlas_width);
    // Display the atlas image, take care of the offset and the zoom.
    ImGui::Image((ImTextureID)param->atlas_texture, g_data.atlas_size,
                 ImVec2(g_data.offset[0], g_data.offset[1]),
                 ImVec2(g_data.offset[0] + 1.0f / g_data.zoom,
                        g_data.offset[1] + 1.0f / g_data.zoom));

    ImGui::SameLine(); // -----------------------------------------------------

    ImGuiWindowFlags windowFlags = ImGuiWindowFlags_MenuBar;
    // ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
    if (ImGui::BeginChild("Params", ImVec2(0, 0), true, windowFlags)) {
      static int tab = 0;
      if (ImGui::BeginMenuBar() || true) {
        if (ImGui::SmallButton("Widget Conf")) {
          tab = 0;
        }
        ImGui::SameLine();
        if (ImGui::SmallButton("User Images")) {
          tab = 1;
        }
        ImGui::EndMenuBar();
      }

      switch (tab) {
      case 0: {
        ImFoxUIEditor_WidgetConf(param, g_data);
      } break;
      case 1: {
        ImFoxUIEditor_UserImage(param, g_data);
      } break;
      }
    }
    ImGui::EndChild();

    //  int         sz        = 32;
    // static ImVec4 colf = ImVec4(1.0f, 1.0f, 0.4f, 1.0f);
    // const ImU32 col = ImColor(colf);
    //  draw_list->AddRectFilled(p, ImVec2(p.x + sz, p.y + sz), col, 10.0f,
    //                           ImDrawCornerFlags_All);

    FoxuiEditor::ShowErrorDialog();
  }
  ImGui::End();
}

namespace FoxuiEditor {

namespace {
const int s_userLogSize = 4096;
char s_userLogBuffer[s_userLogSize];
} // namespace

static void OpenglCheck(bool check, const char *message) {
  if (check) {
    return;
  }

  printf("%s\n", message);
  // TODO: Get OpenGL error here
  exit(1); // TODO: Instead set a global flag to quit nicely the application.
}

static void ProgramCheck(bool check, const char *message) {
  if (check) {
    return;
  }
  printf("%s\n", message);
  // TODO: errno?
  exit(1); // TODO: Instead set a global flag to quit nicely the application.
}

void SetErrorMessage(const char *message) {
  strncpy(s_userLogBuffer, message, s_userLogSize);
  s_userLogBuffer[s_userLogSize - 1] = '\0';
}

void ShowErrorDialog(void) {
  if (strlen(s_userLogBuffer) < 1) {
    return;
  }
  if (ImGui::Begin("FoxuiEditor")) {
    ImGui::PushStyleColor(ImGuiCol_Text,
                          (ImVec4)ImColor::HSV(0.f, 0.6f, 0.85f));
    ImGui::Text("%s", s_userLogBuffer);
    ImGui::PopStyleColor();

    if (ImGui::Button("Close")) {
      s_userLogBuffer[0] = '\0';
    }
  }
  ImGui::End();
}

int IncrementUserImages(foxui_param *param) {
  foxui_rectangle *old = param->user_images;
  param->user_image_count++;
  param->user_images = (foxui_rectangle *)realloc(param->user_images,
                                                  sizeof(*param->user_images) *
                                                      param->user_image_count);
  if (param->user_images == nullptr) {
    param->user_images = old;
    return 0;
  }
  foxui_rectangle *rec = &(param->user_images[param->user_image_count - 1]);
  rec->x = 0.0f;
  rec->y = 0.0f;
  rec->width = 0.0f;
  rec->height = 0.0f;
  return 1;
}

void WindowCreateNew(foxui_param *param) {
  if (ImGui::Begin("New Foxui")) {
    if (ImGui::BeginChild("Create", ImVec2(400, 200), true)) {
      static int atlasWidth = 1024;
      static int atlasHeight = 1024;

      ImGui::PushStyleColor(ImGuiCol_Text,
                            (ImVec4)ImColor::HSV(196.f / 360.0f, 0.42f, 0.93f));
      ImGui::Text("Create new configuration");
      ImGui::PopStyleColor();
      ImGui::Spacing();

      ImGui::InputInt("Atlas Width", &atlasWidth);
      ImGui::InputInt("Atlas Height", &atlasHeight);
      if (ImGui::Button("Create!")) {
        Image_CreateRGBA(&g_data.atlas, atlasWidth, atlasHeight);
        param->atlas_texture = Image_ToGLTexture(&g_data.atlas);
        param->atlas_width = atlasWidth;
        param->atlas_height = atlasHeight;
      }
    }
    ImGui::EndChild();

    // ImGui::Separator();

    ImGui::SameLine();
    if (ImGui::BeginChild("Open42", ImVec2(400, 200), true)) {
      const size_t buffSize = 4096;
      char foxuiParamFile[buffSize] = "./foxui_configuration.so";
      char atlasImageFile[buffSize] = "foxui_atlas.png";

      ImGui::PushStyleColor(ImGuiCol_Text,
                            (ImVec4)ImColor::HSV(196.f / 360.0f, 0.42f, 0.93f));
      ImGui::Text("Open an existing FoxUI configuration");
      ImGui::PopStyleColor();
      ImGui::Spacing();

      ImGui::Text(
          "In order to reload a configuration, you must compile\nthe "
          "generated .c file as a Dynamic library. For\ninstance, under "
          "GNU/Linux:\ngcc -fPIC -c  foxui_configuration.c\ngcc "
          "-shared -o foxui.so foxui_configuration.o");
      ImGui::Spacing();

      ImGui::InputText("Foxui Param file", foxuiParamFile, buffSize);
      ImGui::InputText("Atlas image", atlasImageFile, buffSize);
      if (ImGui::Button("Open!")) {
        // TODO: .so Windows -> .dll
        OpenConfiguration(param, foxuiParamFile, atlasImageFile);
      }
    }
    ImGui::EndChild();
  }
  ImGui::End();

  FoxuiEditor::ShowErrorDialog();
}

// TODO: return -1 in case of error
static int CopyTextureToNewAtlas(foxui_param *param, stbrp_rect *rects,
                                 Image *loadedSurface) {
  const int numImages = param->user_image_count;
  const float oneOverWidth = 1.0f / param->atlas_width;
  const float oneOverHeight = 1.0f / param->atlas_height;
  Image future; // The new packed texture
  int ret;

  // We need to create a new image to not overwrite texture not copied yet.
  ret = Image_CreateRGBA(&future, param->atlas_width, param->atlas_height);
  if (ret < 0) {
    return 0;
  }
  for (int i = 0; i < numImages; ++i) {
    stbrp_rect *rec = &(rects[i]);
    foxui_rectangle dest;
    int ret;

    dest.x = rec->x;
    dest.y = rec->y;
    dest.width = rec->w;
    dest.height = rec->h;
    if (rec->id == (numImages - 1)) {
      // That's the new image we are inserting.
      ret = Image_Copy(loadedSurface, NULL, &future, &dest);
    } else {
      // That's an already atlas'ed image.
      foxui_rectangle src = param->user_images[rec->id];

      src.x *= param->atlas_width;
      src.y *= param->atlas_height;
      src.width *= param->atlas_width;
      src.height *= param->atlas_height;
      ret = Image_Copy(&g_data.atlas, &src, &future, &dest);
    }
    if (ret != 0) {
      printf("Wow a surface was not copied!\n");
      return 0;
    }
  }

  for (int i = 0; i < numImages; ++i) {
    stbrp_rect *rec = &(rects[i]);
    param->user_images[rec->id].x = rec->x * oneOverWidth;
    param->user_images[rec->id].y = rec->y * oneOverHeight;
    param->user_images[rec->id].width = rec->w * oneOverWidth;
    param->user_images[rec->id].height = rec->h * oneOverHeight;
  }

  // TODO: Free previous Image
  // Image_Free(g_data.atlas);
  g_data.atlas = future;
  return 1;
}

// 1. Open Image (using SDL)
// 2. Redo the stb rect pack
// 3. Update any uv that have changed.
// 4. re-write the Image and save it to disk
void ImportSingleImage(foxui_param *param, const char *path) {
  Image img;
  stbrp_context rpcontext;
  stbrp_rect *rects = nullptr;
  stbrp_node *nodes = nullptr;
  const int numNodes = param->atlas_width;
  const int numImages = param->user_image_count + 1;
  int retPacking;
  int ret;

  ret = Image_Load(&img, path);
  if (ret < 0) {
    printf("Unable to load image '%s'\n", path);
  }
  rects = (stbrp_rect *)malloc(sizeof(*rects) * numImages);
  if (rects == nullptr) {
    printf("Not enough memory.\n");
    goto release_memory;
  }
  nodes = (stbrp_node *)malloc(sizeof(*nodes) * numNodes);
  if (nodes == nullptr) {
    printf("Not enough memory.\n");
    goto release_memory;
  }
  stbrp_init_target(&rpcontext, param->atlas_width, param->atlas_height, nodes,
                    numNodes);
  for (int i = 0; i < numImages; ++i) {
    stbrp_rect *rec = &(rects[i]);

    if (i == (numImages - 1)) {
      rec->w = img.width;
      rec->h = img.height;
    } else {
      foxui_rectangle *fox = &(param->user_images[i]);

      rec->w = fox->width * param->atlas_width;
      rec->h = fox->height * param->atlas_height;
    }
    rec->id = i;
    rec->was_packed = 0;
  }
  retPacking = stbrp_pack_rects(&rpcontext, rects, numImages);
  if (retPacking == 0) {
    // printf("Image not big enough to pack everything.\n");
    SetErrorMessage("Image not big enough to pack everything.\n");
    // TODO: Realloc a bigger image
  } else {
    // Now we can commit our work and use the new atlas.
    if (IncrementUserImages(param) == 0) {
      printf("Not enough memory.\n");
      goto release_memory;
    }
    if (CopyTextureToNewAtlas(param, rects, &img)) {
      glDeleteTextures(1, &(param->atlas_texture));
      param->atlas_texture = Image_ToGLTexture(&g_data.atlas);
    }
  }

release_memory:
  free(rects);
  free(nodes);
  // In case of error
  // TODO: Image_Free(img);
}

// TODO: Backup previous configuration
// char* paramFileOld =  File_Path("%s.old", paramFile);
// File_Delete(paramFileOld);
// File_Copy(paramFile, paramFileOld);
// same for atlas...;
int SaveConfiguration(foxui_param *param, const char *paramFile,
                      const char *atlasImage) {
  int ret;

  ret = stbi_write_png(atlasImage, g_data.atlas.width, g_data.atlas.width,
                       g_data.atlas.channels, g_data.atlas.data, 0);
  if (ret == 0) {
    // TODO: Print the path of the image.
    SetErrorMessage("Failed to write image.");
    return -1;
  }

  const std::string output = BuildCConfiguration(param);
  ret = File_Write(paramFile, output.c_str());
  if (ret == 0) {
    return -1;
  }
  return 0;
}

int OpenConfiguration(foxui_param *param, const char *dlFile,
                      const char *atlasImage) {
  Module module;
  void (*foxui_configure_lnf)(foxui_param *);
  int ret;

  ret = Module_Load(dlFile, &module);
  if (ret < 0) {
    return -1;
  }
  *(void **)(&foxui_configure_lnf) =
      Module_GetSymbol(&module, "foxui_configure_lnf");

  if (foxui_configure_lnf) {
    (*foxui_configure_lnf)(param);
  }
  Module_Release(&module);
  if (Image_Load(&g_data.atlas, atlasImage) == 0) {
    param->atlas_texture = Image_ToGLTexture(&g_data.atlas);
  }
  return 0;
}

} // namespace FoxuiEditor
