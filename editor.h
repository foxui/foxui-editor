#pragma once

/// Main function of the editor to call in a loop.
void ImFoxUIEditor(foxui_param *param, foxui_renderdata *render);

namespace FoxuiEditor {

/// Import the image designated as `path` in the atlas texture. Add UVs to a new
/// `foxui_param.user_images` slot.
void ImportSingleImage(foxui_param *param, const char *path);

/// Set the error message to the user
void SetErrorMessage(const char *message);

/// Output the configuration to files, overwrite existing ones.
/// `paramFile` will contains the .c file with the `foxui_configure_lnf`
/// function. `atlasImage` will contains the .png atlas image.
int SaveConfiguration(foxui_param *param, const char *paramFile,
                      const char *atlasImage);

/// Open the specified configuration from the Dynamic Library, return 0 in case
/// of success or < 0 in case of failure. Report the error directly to the user.
int OpenConfiguration(foxui_param *param, const char *dlFile,
                      const char *atlasImage);

/// Increment the number of user image by one. Do reallocation and data
/// initialization to 0.
int IncrementUserImages(foxui_param *param);

/// Internal use. Open the start window when no atlas is opened for edit.
void WindowCreateNew(foxui_param *param);

/// Internal use. Draw the dialog zone to display messages to the user.
void ShowErrorDialog(void);

} // namespace FoxuiEditor
